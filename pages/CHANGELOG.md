## [unreleased]

- Chore:
  - Add support for Ruby 3.2, 3.3, 3.4
  - Drop Ruby 2.7, 3.0 support

## [0.1.0]

- Dependencies:
  - Update to yard [v0.9.27](https://github.com/lsegal/yard/releases/tag/v0.9.27)
    - Move from Redcarpet to CommonMarker markdown provider
    - Move doc syntax from Rdoc to markdown
  - Move dev dependencies from gemspec to gemfile
- Chore:
  - Add support for Ruby 3.1

## [0.0.1] - 1 March 2019

[0.0.1]: https://gitlab.com/noraj/itdis/tags/v0.0.1

- First release.
