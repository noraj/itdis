# Examples

For all examples don't forget to add `require 'itdis'`.

## Help

```
$ itdis -h
ITDIS (Is This Domain In Scope)

Usage:
  itdis [options] (-s <IPs> | -S <file>) (-d <domains> | -D <file>)
  itdis -h | --help
  itdis --version

Options:
  -s <IPs>, --scope <IPs>             Specify the scope: one or multiple IP addresses separated by a comma (,)
  -S <file>, --scope-file <file>      Specify the scope: a file path, the file must contain one IP address per line
  -d <domains>, --domains <domains>   Specify domains to check: one or multiple domains separated by a comma (,)
  -D <file>, --domains-file <file>    Specify domains to check: a file path, the file must contain one domain per line
  --no-color                          Disable colorized output
  --short                             Display in a short format: display only domains in scope
  --debug                             Display arguments
  -h --help                           Show this screen.
  --version                           Show version.

Examples:
  itdis -s 1.1.1.1 -D /tmp/domains.txt
  itdis -s 205.251.242.103,176.32.98.166 -d amazon.com
  itdis -S /tmp/scope.txt -D /tmp/domains.txt --no-color
```

## Quick use

```
$ itdis -s 205.251.242.103,176.32.98.166 -d amazon.com,google.com
amazon.com: [176.32.103.205, 205.251.242.103, 176.32.98.166]
google.com: [216.58.209.238]
```

You can't see here, but in scope domains are in green and out of scope ones are in red.

## Load from file

```
$ itdis -S /tmp/scope.txt -D /tmp/domains.txt
amazon.com: [176.32.103.205, 205.251.242.103, 176.32.98.166]
google.com: [216.58.209.238]
```

With `/tmp/scope.txt`:

```
205.251.242.103
176.32.98.166
```

And `/tmp/domains.txt`:

```
amazon.com
google.com
```

## Short output

Only in scope domains are displayed:

```
$ itdis -s 205.251.242.103,176.32.98.166 -d amazon.com,google.com --short
amazon.com
```

## No color

```
$ itdis -s 205.251.242.103,176.32.98.166 -d amazon.com,google.com --no-color
amazon.com:
  In scope: [176.32.98.166, 205.251.242.103]
  Out of scope: [176.32.103.205]
google.com:
  In scope: []
  Out of scope: [216.58.209.238]
```