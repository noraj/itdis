# Installation

## Production

### Install from rubygems.org

```
$ gem install itdis
```

## Development

It's better to use [rbenv](https://github.com/rbenv/rbenv) to have latests version of ruby and to avoid trashing your system ruby.

### Install from rubygems.org

```
$ gem install --development itdis
```

### Build from git

Just replace `x.x.x` with the gem version you see after `gem build`.

```
$ git clone https://gitlab.com/noraj/itdis.git itdis
$ cd itdis
$ gem install bundler
$ bundler install
$ gem build itdis.gemspec
$ gem install itdis-x.x.x.gem
```

Note: if an automatic install is needed you can get the version with `$ gem build itdis.gemspec | grep Version | cut -d' ' -f4`.

### Run the API in irb without installing the gem

Useful when you want to try your changes without building the gem and re-installing it each time.

```
$ git clone https://gitlab.com/noraj/itdis.git itdis
$ cd itdis
$ irb -Ilib -ritdis
```

Same for the CLI tool:

```
$ ruby -Ilib -ritdis bin/itdis
```

# Publishing

```
$ git tag vx.x.x
$ gem push itdis-x.x.x.gem
```

https://guides.rubygems.org/publishing/