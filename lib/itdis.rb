# frozen_string_literal: true

# @author Alexandre ZANNI <alexandre.zanni@engineer.com>

# Ruby internal (stdlib)
require 'resolv'
# Project internal
require 'itdis/version'

# The class used for resolving domains
class Itdis
  # Load constants
  include ItdisVersion

  # @overload scope
  #   Get the scope
  #   @!macro return_scope
  #     @return [Array<String>] Array of IP addresses that are in the scope
  #   @!macro return_scope
  #   @example
  #     ['192.168.0.42']
  # @overload scope(=scope)
  #   Set the scope
  #   @param scope [Array<String>] Array of IP addresses that are in the scope
  #   @!macro return_scope
  attr_reader :scope

  # @overload domains
  #   Get domains
  #   @!macro return_domains
  #     @return [Array<String>] Array of domains that must be checked
  #   @!macro return_domains
  #   @example
  #     ['example.org', 'example.com']
  # @overload domains=(domains)
  #   Set domains
  #   @param domains [Array<String>] Array of domains that must be checked
  #   @!macro return_domains
  attr_reader :domains

  def scope=(scope)
    raise TypeError, 'scope must be an array' unless scope.is_a?(Array)

    @scope = scope
  end

  def domains=(domains)
    raise TypeError, 'domains must be an array' unless domains.is_a?(Array)

    @domains = domains
  end

  # A new instance of resolver
  # @param scope [Array<String>] see {#scope}
  # @param domains [Array<String>] see {#domains}
  def initialize(scope, domains)
    self.scope = scope
    self.domains = domains
  end

  # Check if the domains of the instance are in the scope
  # @return [Hash] the domain and associated IPs checked.
  #   true if in scope false else.
  # @example
  #   irb(main):001:0> Itdis.new(['127.0.0.1', '205.251.242.103'],['amazon.com']).check
  #   => {"amazon.com"=>{"205.251.242.103"=>true, "176.32.103.205"=>false, "176.32.98.166"=>false}}
  def check
    checked = {}
    @domains.each do |domain|
      ips = {}
      Resolv.each_address(domain.chomp) do |ip|
        is_in_scope = false
        is_in_scope = true if @scope.include?(ip)
        ips.store(ip, is_in_scope)
      end
      checked.store(domain, ips)
    end
    return checked
  end
end
