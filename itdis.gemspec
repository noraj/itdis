# frozen_string_literal: true

require_relative 'lib/itdis/version'

Gem::Specification.new do |s|
  s.name          = 'itdis'
  s.version       = ItdisVersion::VERSION
  s.platform      = Gem::Platform::RUBY
  s.summary       = 'Is This Domain In Scope'
  s.description   = 'Is a small tool that allows you to check if a list of domains you have been provided is in the '
  s.description  += 'scope of your pentest or not.'
  s.authors       = ['Alexandre ZANNI']
  s.email         = 'alexandre.zanni@europe.com'
  s.homepage      = 'https://noraj.gitlab.io/itdis/'
  s.license       = 'MIT'

  s.files         = Dir['bin/*'] + Dir['lib/**/*.rb'] + ['LICENSE.txt']
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.require_paths = ['lib']

  s.metadata = {
    'yard.run'              => 'yard',
    'bug_tracker_uri'       => 'https://gitlab.com/noraj/itdis/issues',
    'changelog_uri'         => 'https://noraj.gitlab.io/itdis/file.CHANGELOG.html',
    'documentation_uri'     => 'https://noraj.gitlab.io/itdis/',
    'homepage_uri'          => 'https://noraj.gitlab.io/itdis/',
    'source_code_uri'       => 'https://gitlab.com/noraj/itdis/tree/master',
    'funding_uri'           => 'https://github.com/sponsors/noraj',
    'rubygems_mfa_required' => 'true'
  }

  s.required_ruby_version = ['>= 3.1.0', '< 4.0']

  s.add_runtime_dependency('docopt', '~> 0.6') # for argument parsing
  s.add_runtime_dependency('paint', '~> 2.3') # for colorized output
end
