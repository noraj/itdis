[![pipeline status](https://gitlab.com/noraj/itdis/badges/master/pipeline.svg)](https://gitlab.com/noraj/itdis/commits/master)

[![Gem version](https://img.shields.io/gem/v/itdis.svg)][rubygems]
[![Gem stable](https://img.shields.io/gem/dv/itdis/stable.svg)][rubygems]
[![Gem latest](https://img.shields.io/gem/dtv/itdis.svg)][rubygems]
[![Gem total download](https://img.shields.io/gem/dt/itdis.svg)][rubygems]
[![Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/img/badges/Rawsec-inventoried-FF5050_flat.svg)](https://inventory.rawsec.ml/tools.html#itdis)

[rubygems]:https://rubygems.org/gems/itdis

# ITDIS (Is This Domain In Scope)

Is a small tool that allows you to check if a list of domains you have been provided is in the scope of your pentest or not.

Name            | Link
---             | ---
Website         | [link](https://noraj.gitlab.io/itdis/)
Documentation   | [link](https://noraj.gitlab.io/itdis/)
Gem             | [link](https://rubygems.org/gems/itdis)
Git repository  | [link](https://gitlab.com/noraj/itdis)
Merge Requests  | [link](https://gitlab.com/noraj/itdis/merge_requests)
Issues          | [link](https://gitlab.com/noraj/itdis/issues)

## Quick help

```
$ itdis -h
ITDIS (Is This Domain In Scope)

Usage:
  itdis [options] (-s <IPs> | -S <file>) (-d <domains> | -D <file>)
  itdis -h | --help
  itdis --version

Options:
  -s <IPs>, --scope <IPs>             Specify the scope: one or multiple IP addresses separated by a comma (,)
  -S <file>, --scope-file <file>      Specify the scope: a file path, the file must contain one IP address per line
  -d <domains>, --domains <domains>   Specify domains to check: one or multiple domains separated by a comma (,)
  -D <file>, --domains-file <file>    Specify domains to check: a file path, the file must contain one domain per line
  --no-color                          Disable colorized output
  --short                             Display in a short format: display only domains in scope
  --debug                             Display arguments
  -h --help                           Show this screen.
  --version                           Show version.

Examples:
  itdis -s 1.1.1.1 -D /tmp/domains.txt
  itdis -s 205.251.242.103,176.32.98.166 -d amazon.com
  itdis -S /tmp/scope.txt -D /tmp/domains.txt --no-color
```

For more help see the documentation.